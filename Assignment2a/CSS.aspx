﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="CSS.aspx.cs" Inherits="Assignment2a.CSS" %>

<asp:Content ContentPlaceHolderID="Mainpage" runat="server"> 
    <div class="jumbotron">
        <h1>STUDY GUIDE</h1>
        <p class="lead">Welcome to study guide of javascript, CSS and SQL</p>
        </div>
    </asp:Content>
<asp:Content ContentPlaceHolderID="Intro" runat="server"> 
    <h3>CSS Intro</h3>
        CSS is a language that describes the style of an HTML document.CSS describes how HTML elements should be displayed.
</asp:Content>
<asp:Content ContentPlaceHolderID="OwnExample" runat="server">      
    <h3>My example</h3>
   body {    background-color: lightblue;}

h1 {
    color: white;
    text-align: center;
}

p {
    font-family: verdana;
    font-size: 20px;
}
</asp:Content>         
<asp:Content ContentPlaceHolderID="FoundExample" runat="server">     
        <h3>Example from web</h3>
        h1 style="background-color:DodgerBlue;">Hello World
        
</asp:Content>     
<asp:Content ContentPlaceHolderID="Links" runat="server">       
    <h3>Links</h3>
  <a href="#">Elements</a>
  <a href="#">Border</a>
  <a href="#">Margin</a>
  <a href="#">Padding</a>
 </asp:Content>
     