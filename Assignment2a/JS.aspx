﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="JS.aspx.cs" Inherits="Assignment2a.JS" %>

<asp:Content ContentPlaceHolderID="Mainpage" runat="server"> 
   <div class="jumbotron">
        <h1>JAVASCRIPT</h1>
        <p class="lead">Welcome to study guide of javascript</p>
        </div>
    </asp:Content>
<asp:Content ContentPlaceHolderID="Intro" runat="server">
    <h3>Javascript Intro </h3>
JavaScript is the programming language of HTML and the Web.JavaScript is easy to learn
   </asp:Content>
<asp:Content ContentPlaceHolderID="OwnExample" runat="server">     
    <h3>Display Date Example</h3>
   onclick="document.getElementById('demo').innerHTML = Date()">
Click me to display Date and Time.
   </asp:Content>

<asp:Content ContentPlaceHolderID="FoundExample" runat="server">
     
        <h3>example from web</h3>
   document.getElementById("demo").innerHTML = "Hello Dolly.";
  
</asp:Content>

<asp:Content ContentPlaceHolderID="Links" runat="server">
    
    <h3>LINKS</h3>
    <a href="#">Array</a>
  <a href="#">While Loop</a>
  <a href="#">for Loop</a>
  <a href="#">objects</a>
       
</asp:Content>