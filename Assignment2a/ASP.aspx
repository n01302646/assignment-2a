﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ASP.aspx.cs" Inherits="Assignment2a.ASP" %>
<asp:Content ContentPlaceHolderID="Mainpage" runat="server"> 
    <div class="jumbotron">
        <h1>ASP</h1>
        <p class="lead">Welcome to study guide of ASP.NET</p>
        </div>
    </asp:Content>
<asp:Content ContentPlaceHolderID="Intro" runat="server"> 
    <h3>ASP Intro</h3>
 ASP stands for Active Server Pages
ASP is a development framework for building web pages.
</asp:Content>

<asp:Content ContentPlaceHolderID="OwnExample" runat="server">
     <h3>Own example</h3>
   The time is @DateTime.Now. 
    <br />output: The time is 10/21/2018 3:47:20 AM
        
</asp:Content>

<asp:Content ContentPlaceHolderID="FoundExample" runat="server">

        <h3>example from web</h3>
    @for i=10 to 21
   @<p>Line @i</p>    
</asp:Content>

<asp:Content ContentPlaceHolderID="Links" runat="server">
    
    <h3>LINKS</h3>
    <a href="#">Array</a><br />
  <a href="#">While Loop</a><br />
  <a href="#">for Loop</a><br />        
</asp:Content>