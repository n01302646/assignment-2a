﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assignment2a._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="Mainpage" runat="server">

    <div class="jumbotron">
        <h1>STUDY GUIDE</h1>
        <p class="lead">Welcome to study guide of javascript, CSS and SQL</p>
        </div>

    <div class="row">
            <div class="col-md-6">
                <h2>Learn tricks</h2>
                <p>
                   see overview explaining a tricky concept in javascript, CSS and SQL
                </p>                
            </div>
            <div class="col-md-6">
                <h2>Own Examples</h2>
                <p>
                    Get an example of a snippet of code i wrote myself.
                </p>                
            </div>
            <div class="col-md-6">
                <h2>Found Example</h2>
                <p>
                    Get an example of a snippet of code i found online.
                </p>                
            </div>
            <div class="col-md-6">
                <h2>Get links to concepts</h2>
                <p>
                  Get links to concepts explaining the topics
                </p>
                
            </div>
        </div>
</asp:Content>
